package DataDrivenAss;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PassingMulipleData {
	

	public static void main(String[] args) throws IOException {
        // TODO Auto-generated method stub

        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demowebshop.tricentis.com/");
        File f = new File("/home/sudharshan/Documents/Testdata.com/Testdata2.xlsx");

        FileInputStream fis=new FileInputStream(f);
        XSSFWorkbook workbook= new XSSFWorkbook(fis);
        XSSFSheet sh=workbook.getSheetAt(1);
        int rows=sh.getPhysicalNumberOfRows();

        for(int i=1;i<rows;i++)
        {
            String firstname=sh.getRow(i).getCell(0).getStringCellValue();
            String lastname=sh.getRow(i).getCell(1).getStringCellValue();
            String email=sh.getRow(i).getCell(2).getStringCellValue();
            String password=sh.getRow(i).getCell(3).getStringCellValue();
            String confirmpassword=sh.getRow(i).getCell(4).getStringCellValue();
            
            
            driver.findElement(By.linkText("Register")).click();
            driver.findElement(By.id("gender-male")).click();
            driver.findElement(By.id("FirstName")).sendKeys(firstname);    
            driver.findElement(By.id("LastName")).sendKeys(lastname);
            driver.findElement(By.id("Email")).sendKeys(email);
            driver.findElement(By.id("Password")).sendKeys(password);
            driver.findElement(By.id("ConfirmPassword")).sendKeys(confirmpassword);
            driver.findElement(By.id("register-button")).click();

        }
    }
	

}
